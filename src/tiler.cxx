#include "tiler.h"

#include "TreeInformation.h"

#include <vtkActor.h>
#include <vtkCellData.h>
#include <vtkCityGMLReader.h>
#include <vtkCompositeDataIterator.h>
#include <vtkDataObject.h>
#include <vtkDataObjectTreeIterator.h>
#include <vtkDirectory.h>
#include <vtkDoubleArray.h>
#include <vtkIncrementalOctreePointLocator.h>
#include <vtkIncrementalOctreeNode.h>
#include <vtkJPEGReader.h>
#include <vtkLogger.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkOBJReader.h>
#include <vtkPNGReader.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkTexture.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtksys/SystemTools.hxx>


#include <algorithm>
#include <set>

using namespace vtksys;

//------------------------------------------------------------------------------
void SetField(vtkDataObject* obj, const char* name, const char* value)
{
  vtkFieldData* fd = obj->GetFieldData();
  if (!fd)
  {
    vtkNew<vtkFieldData> newfd;
    obj->SetFieldData(newfd);
    fd = newfd;
  }
  vtkNew<vtkStringArray> sa;
  sa->SetNumberOfTuples(1);
  sa->SetValue(0, value);
  sa->SetName(name);
  fd->AddArray(sa);
}

//------------------------------------------------------------------------------
std::string GetFieldAsString(vtkDataObject* obj, const char* name)
{
  vtkFieldData* fd = obj->GetFieldData();
  if (!fd)
  {
    return std::string();
  }
  vtkStringArray* sa = vtkStringArray::SafeDownCast(fd->GetAbstractArray(name));
  if (! sa)
  {
    return std::string();
  }
  return sa->GetValue(0);
}


//------------------------------------------------------------------------------
std::array<double, 3> ReadOBJOffset(const char* comment)
{
  std::array<double, 3> translation = {0, 0, 0};
  if (comment)
  {
    std::istringstream istr(comment);
    std::array<std::string, 3> axesNames = {"x", "y", "z"};
    for (int i = 0; i < 3; ++i)
    {
      std::string axis;
      std::string s;
      istr >> axis >> s >> translation[i];
      if (istr.fail())
      {
        vtkLog(WARNING, "Cannot read axis " << axesNames[i] << " from comment.");
      }
      if (axis != axesNames[i])
      {
        vtkLog(WARNING, "Invalid axis " << axesNames[i] << ": " << axis);
      }
    }
  }
  else
  {
    vtkLog(WARNING, "nullptr comment.");
  }
  return translation;
}

//------------------------------------------------------------------------------
std::string GetOBJTextureFileName(const std::string& file)
{
  std::string fileNoExt = SystemTools::GetFilenameWithoutExtension(file);
  return fileNoExt + ".png";
}

//------------------------------------------------------------------------------
void SaveLevel(int level, vtkPolyData* poly)
{
  std::ostringstream ostr;
  ostr << "level_" << level << ".vtp";
  vtkNew<vtkXMLPolyDataWriter> writer;
  writer->SetInputDataObject(poly);
  writer->SetFileName(ostr.str().c_str());
  writer->Write();
}


//------------------------------------------------------------------------------
std::array<double, 6> ReadOBJBuildings(
  int numberOfBuildings,
  const std::vector<std::string>& files,
  std::vector<vtkSmartPointer<vtkCompositeDataSet>>& buildings, std::array<double, 3>& fileOffset)
{
  std::array<double,6> wholeBB = {
    std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest(),
    std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest(),
    std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest()};
  for (int i = 0; i < files.size() && i < numberOfBuildings; ++i)
  {
    vtkNew<vtkOBJReader> reader;
    reader->SetFileName(files[i].c_str());
    reader->Update();
    if (i == 0)
    {
      fileOffset = ReadOBJOffset(reader->GetComment());
    }
    auto building = vtkSmartPointer<vtkMultiBlockDataSet>::New();
    auto polyData = reader->GetOutput();
    building->SetBlock(0, polyData);
    std::string textureFileName = GetOBJTextureFileName(files[i]);
    SetField(polyData, "texture_uri", textureFileName.c_str());
    buildings.push_back(building);
    double bb[6];
    buildings[i]->GetBounds(bb);
    wholeBB = TreeInformation::ExpandBounds(&wholeBB[0], bb);
  }
  return wholeBB;
}

vtkSmartPointer<vtkImageReader2> SetupTextureReader(
  const std::string& texturePath)
{
  std::string ext = SystemTools::GetFilenameLastExtension(texturePath);
  if (ext == ".png")
  {
    return vtkSmartPointer<vtkPNGReader>::New();
  }
  else if (ext == ".jpg")
  {
    return vtkSmartPointer<vtkJPEGReader>::New();
  }
  else
  {
    vtkLog(ERROR, "Invalid type for texture file: " << texturePath);
    return nullptr;
  }
}


void AddTextures(
  const std::string& path,
  std::vector<vtkSmartPointer<vtkCompositeDataSet>>& buildings,
  std::vector<size_t>& buildingActorStart,
  std::vector<vtkSmartPointer<vtkActor>>& actors,
  vtkRenderer* renderer, bool meshOnly)
{
  for (int i = 0; i < buildings.size(); ++i)
  {
    auto it = vtk::TakeSmartPointer(buildings[i]->NewIterator());
    buildingActorStart.push_back(actors.size());
    for (it->InitTraversal(); !it->IsDoneWithTraversal(); it->GoToNextItem())
    {
      auto surface = it->GetCurrentDataObject();
      if (! vtkPolyData::SafeDownCast(surface))
      {
        vtkLog(WARNING, "Expecting vtkPolyData but got: " << surface->GetClassName());
      }
      vtkNew<vtkPolyDataMapper> mapper;
      mapper->SetInputDataObject(surface);

      auto actor = vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(mapper);
      actors.push_back(actor);
      std::string textureFileName = GetFieldAsString(surface, "texture_uri");
      if (! meshOnly && ! textureFileName.empty())
      {
        std::string texturePath = path + "/" + textureFileName;
        auto textureReader = SetupTextureReader(texturePath);
        textureReader->SetFileName(texturePath.c_str());
        vtkNew<vtkTexture> texture;
        texture->SetInputConnection(textureReader->GetOutputPort());
        actor->SetTexture(texture);
      }
      renderer->AddActor(actor);
    }
  }
  buildingActorStart.push_back(actors.size());
}


//------------------------------------------------------------------------------
/**
 * Add building centers to the octree
 * and create actors to add to the renderer.
 */
vtkSmartPointer<vtkIncrementalOctreePointLocator>
BuildOctree(
  std::vector<vtkSmartPointer<vtkCompositeDataSet>>& buildings,
  const std::array<double, 6>& wholeBB,
  int buildingsPerTile)
{
  vtkNew<vtkPoints> points;
  points->SetDataTypeToDouble();
  vtkNew<vtkIncrementalOctreePointLocator> octree;
  octree->SetMaxPointsPerLeaf(buildingsPerTile);
  octree->InitPointInsertion(points, &wholeBB[0]);

  //TreeInformation::PrintBounds("octreeBB", &wholeBB[0]);
  for (int i = 0; i < buildings.size(); ++i)
  {
    double bb[6];
    buildings[i]->GetBounds(bb);
    double center[3] =
      {(bb[0] + bb[1]) / 2.0, (bb[2] + bb[3])/2, (bb[4] + bb[5])/2};
    octree->InsertNextPoint(center);
  }
  return octree;
}

//------------------------------------------------------------------------------
std::array<double, 6> ReadOBJBuildingsWithTexture(
  int numberOfBuildings, int /*lod*/,
  const std::vector<std::string>& files,
  const std::vector<double>& inputOffset,
  vtkRenderer* renderer, bool meshOnly,

  std::vector<vtkSmartPointer<vtkCompositeDataSet>>& buildings,
  std::vector<size_t>& buildingActorStart,
  std::vector<vtkSmartPointer<vtkActor>>& actors,
  std::array<double, 3>& offset)
{
  std::array<double, 3> fileOffset;
  std::array<double, 6> wholeBB = ReadOBJBuildings(
    numberOfBuildings, files, buildings, fileOffset);
  vtkLog(INFO, "FileOffset: [" << fileOffset[0] << ", "
         << fileOffset[1] << ", " << fileOffset[2] << "]");
  vtkLog(INFO, "InputOffset: [" << inputOffset[0] << ", "
         << inputOffset[1] << ", " << inputOffset[2] << "]");
  std::transform(fileOffset.begin(), fileOffset.end(), inputOffset.begin(),
                 offset.begin(), std::plus<double>());
  std::string path = SystemTools::GetFilenamePath(files[0]);
  AddTextures(path, buildings, buildingActorStart, actors, renderer, meshOnly);
  return wholeBB;
}

//------------------------------------------------------------------------------
std::array<double, 6> ReadCityGMLBuildingsWithTexture(
  int numberOfBuildings, int lod,
  const std::vector<std::string>& files,
  const std::vector<double>& inputOffset,
  vtkRenderer* renderer, bool meshOnly,

  std::vector<vtkSmartPointer<vtkCompositeDataSet>>& buildings,
  std::vector<size_t>& buildingActorStart,
  std::vector<vtkSmartPointer<vtkActor>>& actors,
  std::array<double, 3>& offset)
{
  if (files.size() > 1)
  {
    vtkLog(WARNING, "Can only process one CityGML file for now.");
  }
  vtkNew<vtkCityGMLReader> reader;
  reader->SetFileName(files[0].c_str());
  reader->SetNumberOfBuildings(numberOfBuildings);
  reader->SetLOD(lod);
  reader->Update();

  vtkMultiBlockDataSet* root = reader->GetOutput();
  std::array<double, 6> wholeBB;
  root->GetBounds(&wholeBB[0]);

  // translate the buildings so that the minimum wholeBB is at 0,0,0
  offset = {{wholeBB[0], wholeBB[2], wholeBB[4]}};  
  vtkNew<vtkTransformFilter> f;
  vtkNew<vtkTransform> t;
  t->Identity();
  t->Translate(-offset[0], -offset[1], -offset[2]);
  f->SetTransform(t);
  f->SetInputConnection(reader->GetOutputPort());
  f->Update();
  vtkMultiBlockDataSet* transformedRoot =
    vtkMultiBlockDataSet::SafeDownCast(f->GetOutput());
  
  auto buildingIt = vtk::TakeSmartPointer(transformedRoot->NewTreeIterator());
  buildingIt->VisitOnlyLeavesOff();
  buildingIt->TraverseSubTreeOff();
  for (buildingIt->InitTraversal();
       ! buildingIt->IsDoneWithTraversal(); buildingIt->GoToNextItem())
  {
    auto building = vtkMultiBlockDataSet::SafeDownCast(buildingIt->GetCurrentDataObject());
    if (! building)
    {
      buildings.clear();
      return wholeBB;
    }
    buildings.push_back(building);
  }
  std::string path = SystemTools::GetFilenamePath(files[0]);
  AddTextures(path, buildings, buildingActorStart, actors, renderer, meshOnly);

  return wholeBB;
}

//------------------------------------------------------------------------------
using ReaderType = std::array<double, 6> (*)(
  int numberOfBuildings, int lod,
  const std::vector<std::string>& files,
  const std::vector<double>& inputOffset,
  vtkRenderer* renderer, bool meshOnly,
  std::vector<vtkSmartPointer<vtkCompositeDataSet>>& buildings,
  std::vector<size_t>& buildingActorStart,
  std::vector<vtkSmartPointer<vtkActor>>& actors,
  std::array<double, 3>& offset);
std::map<std::string, ReaderType> READER = {
  {".obj", ReadOBJBuildingsWithTexture},
  {".gml", ReadCityGMLBuildingsWithTexture}
};


//------------------------------------------------------------------------------
bool isSupported(const char* file)
{
  std::string ext = SystemTools::GetFilenameExtension(file);
  return READER.find(ext) != READER.end();
}

//------------------------------------------------------------------------------
std::vector<std::string> getFiles(const std::vector<std::string>& input)
{
  std::vector<std::string> files;
  for (std::string name: input)
  {
    if (SystemTools::FileExists(name.c_str(), false/*isFile*/))
    {
      if (SystemTools::FileIsDirectory(name))
      {
        // add all supported files from the directory
        vtkNew<vtkDirectory> dir;
        if (! dir->Open(name.c_str()))
        {
          vtkLog(WARNING, "Cannot open directory: " << name);
        }
        for (int i = 0; i < dir->GetNumberOfFiles(); ++i)
        {
          const char* file = dir->GetFile(i);
          if (! SystemTools::FileIsDirectory(file) && isSupported(file))
          {
            files.push_back( name + "/" + file);
          }
        }
      }
      else
      {
        files.push_back(name);
      }
    }
    else
    {
      vtkLog(WARNING, "No such file or directory: " << name);
    }
  }
  return files;
}

//------------------------------------------------------------------------------
void tiler(const std::vector<std::string>& input, const std::string& output,
           int numberOfBuildings, int buildingsPerTile, int lod,
           const std::vector<double>& inputOffset,
           bool jsonOnly, bool meshOnly,
           const int utmZone, const std::string& utmHemisphere)
{
  vtkNew<vtkNamedColors> colors;
  std::vector<std::string> files = getFiles(input);
  if (files.empty())
  {
    vtkLog(ERROR, "No valid input files");
    return;
  }
  vtkLog(INFO, "Parsing " << files.size() << " files...")
  vtkNew<vtkRenderer> renderer;
  auto renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize(640, 480);

  std::vector<vtkSmartPointer<vtkCompositeDataSet>> buildings;
  std::vector<size_t> buildingActorStart;
  std::vector<vtkSmartPointer<vtkActor>> actors;
  std::array<double, 3> offset = {0, 0, 0};
  std::array<double, 6> wholeBB = READER[SystemTools::GetFilenameExtension(files[0])](
    numberOfBuildings, lod, files, inputOffset, renderer, meshOnly,
    buildings, buildingActorStart, actors, offset);
  if (buildings.empty())
  {
    vtkLog(ERROR, "No buildings read from the input file. "
           "Maybe buildings are on a different LOD. Try changing --lod parameter.");
    return;
  }
  vtkLog(INFO, "Processing " << buildings.size() << " buildings and "
         << actors.size() << " actors...");

  vtkSmartPointer<vtkIncrementalOctreePointLocator> octree =
    BuildOctree(buildings, wholeBB, buildingsPerTile);
  TreeInformation treeInformation(
    octree->GetRoot(), octree->GetNumberOfNodes(), buildings,
    buildingActorStart, offset,
    actors, renderWindow, output, utmZone, utmHemisphere);
  treeInformation.Compute();
  vtkLog(INFO, "Generating tileset.json for "
         << octree->GetNumberOfNodes() << " nodes...");
  treeInformation.Generate3DTiles(output + "/tileset.json");
  // debug - save poly data for each level of the tree.
  int numberOfLevels = octree->GetNumberOfLevels();
  for (int level = 0; level < numberOfLevels; ++level)
  {
    vtkNew<vtkPolyData> octreePoly;
    octree->GenerateRepresentation(
      level, octreePoly, &TreeInformation::GetNodeBounds, &treeInformation);
    treeInformation.AddGeometricError(octreePoly);
    SaveLevel(level, octreePoly);
  }
  renderWindow->Render();
  if (! jsonOnly)
  {
    treeInformation.SaveGLTF();
  }
}
