#! /bin/bash

PARAMS=""
while (( "$#" )); do
  case "$1" in
    -d|--debug)
      DEBUG=0
      shift
      ;;
    -c|--city)
      if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
        CITY=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      echo "$0 -c[|--city] jacksonville|berlin|nyc -d[--debug]"
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

# generate 3D Tiles
cd ~/projects/tiler/build || exit

if [ "${CITY}" = "jacksonville" ]; then
    dir=3d-tiles
    rm -rf ${dir}
    mkdir ${dir}
    ./bin/tiler ../../../data/CORE3D/Jacksonville/building_*building*.obj -o 3d-tiles --utm_zone 17 --utm_hemisphere N -b 100
elif [ "${CITY}" = "berlin" ]; then
    dir=berlin-3d-tiles
    rm -rf $dir
    mkdir $dir
    ./bin/tiler ../../../data/Berlin-3D/Charlottenburg-Wilmersdorf/citygml.gml -o berlin-3d-tiles --utm_zone 33 --utm_hemisphere N -b 100 --mesh_only --number_of_buildings 20
elif [ "${CITY}" = "nyc" ]; then
    dir=ny-3d-tiles
    rm -rf $dir
    mkdir $dir
else
    echo "$0 -c[|--city] jacksonville|berlin|nyc -d[--debug]"
    exit 1
fi

cd ${dir} || exit
# convert to glb
echo "Converting to glb..."
find . -name '*.gltf' -exec bash -c 'nodejs ~/external/gltf-pipeline/bin/gltf-pipeline.js -i ${0} -o ${0%.*}.glb' {} \;
if [ ! ${DEBUG} ]; then
    echo "Deleting gltf and bin files..."
    find . -name '*.gltf' -exec rm {} \;
    find . -name '*.bin' -exec rm {} \;
fi
# convert to b3dm
echo "Converting to b3dm..."
find . -name '*.glb' -exec bash -c 'nodejs ~/external/3d-tiles-tools/tools/bin/3d-tiles-tools.js glbToB3dm ${0} ${0%.*}.b3dm' {} \;
if [ ! ${DEBUG} ]; then
    echo "Deleting glb files..."
    find . -name '*.glb' -exec rm {} \;
fi
